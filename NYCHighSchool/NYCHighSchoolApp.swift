//
//  NYCHighSchoolApp.swift
//  NYCHighSchool
//
//  Created by Akshitha atmakuri on 2/8/24.
//

import SwiftUI

@main
struct NYCHighSchoolApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolsListView()
        }
    }
}
