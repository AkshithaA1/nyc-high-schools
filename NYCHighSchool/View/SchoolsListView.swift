//
//  SchoolsListView.swift
//  NYCHighSchool
//
//  Created by Akshitha atmakuri on 2/8/24.
//

import SwiftUI

struct SchoolsListView: View {
    @StateObject private var viewModel = SchoolsListViewModel()
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(viewModel.search(), id: \.dbn) { school in
                    NavigationLink {
                        SchoolDetailScreen(school: school)
                    } label: {
                        SchoolListItem(school: school)
                    }
                }
            }
            .listStyle(.plain)
            .navigationTitle("NYC Schools")
            .navigationBarTitleDisplayMode(.inline)
            .task {
                await viewModel.fetchData()
            }
            .searchable(text: $viewModel.searchString, tokens: $viewModel.searchTokens, placement: .automatic, prompt: "Search") { token in
                token.tokenView
            }
            .toolbar {
                navigationTitle
            }
        }
    }
    
    @ToolbarContentBuilder
    var navigationTitle: some ToolbarContent {
        ToolbarItem(placement: .principal) {
            Text("")
        }
        ToolbarItem(placement: .navigationBarLeading) {
            Text("NYC schools")
                .font(.system(.largeTitle, design: .rounded, weight: .bold))
                .foregroundColor(.publicNavy)
                .padding(.bottom, 6)
        }
    }
}

struct SchoolsListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsListView()
    }
}
