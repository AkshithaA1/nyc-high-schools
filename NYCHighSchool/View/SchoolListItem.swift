//
//  SchoolListItem.swift
//  NYCHighSchool
//
//  Created by Akshitha atmakuri on 2/8/24.
//

import SwiftUI

struct SchoolListItem: View {
    let school: School
    var body: some View {
        Text(school.name)
            .lineLimit(1)
            .font(.system(.title3))
            .foregroundColor(.black)
    }
}

struct SchoolListItem_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListItem(school: School.sampleData[0])
    }
}
