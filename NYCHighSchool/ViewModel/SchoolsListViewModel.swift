//
//  SchoolsListViewModel.swift
//  NYCHighSchool
//
//  Created by Akshitha atmakuri on 2/8/24.
//

import SwiftUI

final class SchoolsListViewModel: ObservableObject {
    @Published private(set) var schools = [School]()
    @Published var sortOrder = SortOrder.name {
        didSet {
            schools.sort(by: sortOrder.predicate)
        }
    }
    
    @Published var searchString = ""
    @Published var searchTokens = [SearchToken]()
    
    private let apiManager: any APIManagerProtocol
    
    init(apiManager: any APIManagerProtocol = APIManager.shared) {
        self.apiManager = apiManager
    }
    
    func search() -> [School] {
        if !searchString.isEmpty {
            return schools.filter { $0.name.lowercased().contains(searchString.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)) }
        } else {
            return schools
        }
    }
    
    @MainActor
    func fetchData() async {
        async let callSchools: Void = fetchSchools()
        let _ = await [callSchools]
    }
    
    @MainActor
    func fetchSchools() async {
        do {
            schools = try await apiManager.request(session: .shared, .schoolDirectoryURL)
            schools.sort(by: sortOrder.predicate)
        } catch {
            dump(error)
        }
    }
}

enum SortOrder: String, CaseIterable, Identifiable {
    case name
    case size
    
    var id: Self { self }
    
    var predicate: (School, School) -> Bool {
        switch self {
            case .name:
                return { $0.name < $1.name }
            case .size:
                return { $0.totalStudents ?? 0 < $1.totalStudents ?? 0 }
        }
    }
}

protocol Tokenizable: RawRepresentable, CaseIterable, Identifiable where ID == String, RawValue == String {
    var tokenView: Text { get }
}

extension Tokenizable {
    static var allTokens: [SearchToken] {
        allCases.map(\.searchToken)
    }
    
    var searchToken: SearchToken {
        SearchToken(token: self)
    }
    
    var tokenView: Text {
        Text(rawValue)
    }
    
    var id: String { rawValue }
}

struct SearchToken: Identifiable {
    let token: any Tokenizable
    
    var tokenView: some View {
        token.tokenView
    }
    
    var id: String {
        token.id
    }
}

enum Borough: String {
    case Bronx
    case Brooklyn
    case Manhattan
    case Queens
    case StatenIsland = "Staten Island"
}

extension Borough: Tokenizable {}

enum OtherTokenEnum: String, Tokenizable {
    case other
}

