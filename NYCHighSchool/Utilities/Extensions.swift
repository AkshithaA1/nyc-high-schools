//
//  SwiftUIView.swift
//  NYCHighSchool
//
//  Created by Akshitha atmakuri on 2/8/24.
//

import SwiftUI

extension String {
    // Constants
    static let schoolDirectoryURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
}
