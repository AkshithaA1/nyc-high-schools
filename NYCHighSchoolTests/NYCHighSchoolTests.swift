//
//  NYCHighSchoolTests.swift
//  NYCHighSchoolTests
//
//  Created by Akshitha atmakuri on 2/8/24.
//

import XCTest
@testable import NYCHighSchool

final class NYCHighSchoolTests: XCTestCase {

    var schoolsListViewModel: SchoolsListViewModel?
    var schools: [School]?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.schoolsListViewModel = SchoolsListViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.schoolsListViewModel = nil
        self.schools = nil
    }
    
    func testFetchData() async {
        await self.schoolsListViewModel?.fetchData()
        XCTAssert(!(schoolsListViewModel?.schools.isEmpty)!)
    }
    
    func testSearchWithModel() async {
        await self.schoolsListViewModel?.fetchData()
        schoolsListViewModel?.searchString = "Model"
        self.schools = schoolsListViewModel?.search()
        XCTAssert(!(schoolsListViewModel?.schools.isEmpty)!)
        XCTAssertEqual(self.schools!.count == 1, true)
    }
    
    func testSearchWithLaw() async {
        await self.schoolsListViewModel?.fetchData()
        schoolsListViewModel?.searchString = "Law"
        self.schools = schoolsListViewModel?.search()
        XCTAssert(!(schoolsListViewModel?.schools.isEmpty)!)
        XCTAssertEqual(self.schools!.count == 11, true)
    }

}
